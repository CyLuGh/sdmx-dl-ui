﻿using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using ReactiveUI;
using sdmx_dl_ui.ViewModels;

namespace sdmx_dl_ui.Views
{
    public partial class DimensionSelectorView
    {
        public DimensionSelectorView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( DimensionSelectorView view , DimensionSelectorViewModel viewModel ,
            CompositeDisposable disposables )
        {
            view.TextBlockDimensionLabel.Text = viewModel.Label;

            view.Bind( viewModel ,
                    vm => vm.SearchText ,
                    v => v.TextBoxSearch.Text )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.Codes ,
                    v => v.ItemsControlCodes.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind(viewModel,
                    vm => vm.HasSelection,
                    v => v.ButtonClear.Visibility,
                    b => b ? Visibility.Visible : Visibility.Collapsed)
                .DisposeWith(disposables);

            view.BindCommand(viewModel,
                    vm => vm.ClearSelectionCommand,
                    v => v.ButtonClear)
                .DisposeWith(disposables);
        }
    }
}
