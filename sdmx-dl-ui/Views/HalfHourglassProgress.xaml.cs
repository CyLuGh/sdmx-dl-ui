﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace sdmx_dl_ui.Views
{
    public partial class HalfHourglassProgress
    {
        public HalfHourglassProgress()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty StrokeProperty = DependencyProperty.Register(
            "Stroke" , typeof( Brush ) , typeof( HalfHourglassProgress ) , new FrameworkPropertyMetadata( Brushes.Gray ) );

        public Brush Stroke
        {
            get => (Brush) GetValue( StrokeProperty );
            set => SetValue( StrokeProperty , value );
        }
    }
}
