﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using sdmx_dl_ui.ViewModels;

namespace sdmx_dl_ui.Views.Infrastructure
{
    public static class DataGridExtensions
    {
        public static void BuildColumns( this DataGrid dataGrid , IEnumerable<SeriesViewModel> series )
        {
            dataGrid.AutoGenerateColumns = false;
            dataGrid.Columns.Clear();

            dataGrid.Columns.Add( new DataGridTextColumn
            {
                Header = "Period" ,
                IsReadOnly = true ,
                Binding = new Binding( "Period" ) { StringFormat = "yyyy-MM-dd" }
            } );

            foreach ( var sr in series )
            {
                var column = new DataGridTextColumn
                {
                    Header = sr.Name ,
                    IsReadOnly = true ,
                    Binding = new Binding( sr.MappedName ),
                    ElementStyle = (Style) dataGrid.TryFindResource( "RightAlignCellTextBlockStyle" )
                };
                dataGrid.Columns.Add( column );
            }
        }

        public static void BuildColumns( this DataGrid dataGrid , IDictionary<string , string> mappings )
        {
            dataGrid.AutoGenerateColumns = false;
            dataGrid.Columns.Clear();

            dataGrid.Columns.Add( new DataGridTextColumn
            {
                Header = "Period" ,
                IsReadOnly = true ,
                Binding = new Binding( "Period" ) { StringFormat = "yyyy-MM-dd" }
            } );

            foreach ( var mapping in mappings )
            {
                var column = new DataGridTextColumn
                {
                    Header = mapping.Key ,
                    IsReadOnly = true ,
                    Binding = new Binding( mapping.Value )
                };
                dataGrid.Columns.Add( column );
            }
        }
    }
}
