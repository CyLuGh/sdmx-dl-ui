﻿using System;
using System.Windows;
using GongSolutions.Wpf.DragDrop;
using sdmx_dl_ui.ViewModels;

namespace sdmx_dl_ui.Views.Infrastructure
{
    public class KeyDragHandler : IDragSource
    {
        private readonly ScriptsViewModel _scriptsViewModel;

        public KeyDragHandler( ScriptsViewModel scriptsViewModel )
        {
            _scriptsViewModel = scriptsViewModel;
        }

        public void StartDrag( IDragInfo dragInfo )
        {
            var key = _scriptsViewModel.FullExport
                ? $"{_scriptsViewModel.SelectedSource.Name} {_scriptsViewModel.SelectedFlow.Ref} {_scriptsViewModel.ResultingKey}"
                : _scriptsViewModel.ResultingKey;

            dragInfo.Data = key;
            dragInfo.DataFormat = DataFormats.GetDataFormat( DataFormats.Text );
            
            dragInfo.Effects = dragInfo.Data != null ? DragDropEffects.Copy | DragDropEffects.Move : DragDropEffects.None;
        }

        public bool CanStartDrag( IDragInfo dragInfo )
            => true;

        public void Dropped( IDropInfo dropInfo )
        {

        }

        public void DragDropOperationFinished( DragDropEffects operationResult , IDragInfo dragInfo )
        {

        }

        public void DragCancelled()
        {

        }

        public bool TryCatchOccurredException( Exception exception )
        {
            _scriptsViewModel.MessageQueue.Enqueue( $"Couldn't drop key: {exception.Message}" );
            return true;
        }

    }
}
