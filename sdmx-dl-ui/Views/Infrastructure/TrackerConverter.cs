﻿using System;
using System.Windows.Data;
using OxyPlot;

namespace sdmx_dl_ui.Views.Infrastructure
{
    public class TrackerConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert( object value , Type targetType , object parameter , System.Globalization.CultureInfo culture )
        {
            switch ( parameter.ToString() )
            {
                case "Period":
                {
                    if ( value is double )
                    {
                        var d = (double) value;
                        return OxyPlot.Axes.DateTimeAxis.ToDateTime( d );
                    }
                    if ( value is DateTime )
                    {
                        var d = (DateTime) value;
                        return d;
                    }
                }
                break;

                case "Foreground":
                {
                    if ( value is OxyColor )
                    {
                        var color = (OxyColor) value;
                        return ( PerceivedBrightness( color ) > 130 ) ? System.Windows.Media.Brushes.Black : System.Windows.Media.Brushes.White;
                    }
                }
                break;

                case "TrackerLine":
                {
                    if ( value is bool )
                        return ( (bool) value ) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
                }
                break;
            }

            return null;
        }

        private int PerceivedBrightness( OxyColor c )
        {
            return (int) Math.Sqrt(
            c.R * c.R * .299 +
            c.G * c.G * .587 +
            c.B * c.B * .114 );
        }

        public object ConvertBack( object value , Type targetType , object parameter , System.Globalization.CultureInfo culture )
        {
            return null;
        }

        #endregion IValueConverter Members
    }
}
