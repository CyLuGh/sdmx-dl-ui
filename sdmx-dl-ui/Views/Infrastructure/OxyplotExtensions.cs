﻿using OxyPlot;
using OxyPlot.Axes;
using System.Windows.Media;
using sdmx_dl_ui.Models;

namespace sdmx_dl_ui.Views.Infrastructure
{
    public static class OxyplotExtensions
    {
        private static OxyPlot.Wpf.OxyColorConverter ColorConverter { get; } = new OxyPlot.Wpf.OxyColorConverter();

        public static void Configure( this OxyPlot.Wpf.PlotView plotView , bool displayLegend = true , bool hideAxes = false )
        {
            var bodyBrush = (Brush) plotView.TryFindResource( "MaterialDesignBody" ) ?? Brushes.DarkGray;
            var dividerBrush = (Brush) plotView.TryFindResource( "MaterialDesignDivider" ) ?? Brushes.Gray;

            var hoverController = new PlotController();
            hoverController.Bind( new OxyMouseEnterGesture() , PlotCommands.HoverPointsOnlyTrack );
            plotView.Controller = hoverController;

            var model = new PlotModel();
            model.PlotAreaBorderThickness = new OxyThickness( 0 );
            model.AddAxis( new DateTimeAxis { Position = AxisPosition.Bottom , Key = "MAIN_X_AXIS" } , bodyBrush , dividerBrush );
            model.AddAxis( new LinearAxis { Position = AxisPosition.Left , Key = "MAIN_Y_AXIS" } , bodyBrush , dividerBrush );

            if ( hideAxes )
            {
                foreach ( var axis in model.Axes )
                {
                    axis.AxislineThickness = 0;
                    axis.MajorTickSize = 0;
                    axis.IsAxisVisible = false;
                }

                plotView.Background = Brushes.Transparent;
            }

            model.LegendPlacement = LegendPlacement.Outside;
            model.LegendPosition = LegendPosition.BottomCenter;
            model.LegendOrientation = LegendOrientation.Horizontal;
            model.IsLegendVisible = displayLegend;

            plotView.Model = model;
        }

        /// <summary>
        /// Configures and adds an axis to PlotModel
        /// </summary>
        public static void AddAxis( this PlotModel plotModel , Axis axis , Brush axisBrush , Brush gridBrush , LineStyle ls = LineStyle.Dot )
        {
            axis.ConfigureAxis( axisBrush , gridBrush , ls );
            plotModel.Axes.Add( axis );
        }

        /// <summary>
        /// Configures axis look
        /// </summary>
        public static void ConfigureAxis( this Axis axis , Brush axisBrush , Brush gridBrush , LineStyle ls = LineStyle.Dot )
        {
            axis.AxislineThickness = 1;
            axis.AxislineStyle = LineStyle.Solid;
            axis.AxislineColor = (OxyColor) ColorConverter.ConvertBack( axisBrush , typeof( OxyColor ) , null , null );
            axis.MinorTickSize = 0; // axis.ShowMinorTicks = false; being deprecated
            axis.TicklineColor = (OxyColor) ColorConverter.ConvertBack( axisBrush , typeof( OxyColor ) , null , null );
            axis.MajorGridlineStyle = ls;
            axis.MajorGridlineColor = (OxyColor) ColorConverter.ConvertBack( gridBrush , typeof( OxyColor ) , null , null );
            axis.TextColor = (OxyColor) ColorConverter.ConvertBack( axisBrush , typeof( OxyColor ) , null , null );
        }

        public static void AddLineSeries( this PlotModel plotModel , ChartSeries chartSeries , Brush brush )
        {
            var series = new OxyPlot.Series.LineSeries()
            {
                Title = chartSeries.Title ,
                Color = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                MarkerFill = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                MarkerStroke = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                ItemsSource = chartSeries ,
                DataFieldX = "Period" ,
                DataFieldY = "Value" ,
                CanTrackerInterpolatePoints = false ,
                Tag = chartSeries
            };

            plotModel.Series.Add( series );
        }

        public static void AddStepLineSeries( this PlotModel plotModel , ChartSeries chartSeries , Brush brush )
        {
            var series = new OxyPlot.Series.StairStepSeries()
            {
                Title = chartSeries.Title ,
                Color = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                MarkerFill = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                MarkerStroke = (OxyColor) ColorConverter.ConvertBack( brush , typeof( OxyColor ) , null , null ) ,
                ItemsSource = chartSeries ,
                DataFieldX = "Period" ,
                DataFieldY = "Value" ,
                CanTrackerInterpolatePoints = false ,
                Tag = chartSeries
            };

            plotModel.Series.Add( series );
        }
    }
}
