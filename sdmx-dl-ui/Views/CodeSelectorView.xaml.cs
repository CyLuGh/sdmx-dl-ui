﻿using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using sdmx_dl_ui.ViewModels;

namespace sdmx_dl_ui.Views
{
    public partial class CodeSelectorView
    {
        public CodeSelectorView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( CodeSelectorView view , CodeSelectorViewModel viewModel ,
            CompositeDisposable disposables )
        {
            view.CheckBoxCode.Content = viewModel.Label;

            view.Bind( viewModel ,
                    vm => vm.IsSelected ,
                    v => v.CheckBoxCode.IsChecked )
                .DisposeWith( disposables );
        }
    }
}
