﻿using System.Reactive;
using ReactiveUI;
using sdmx_dl_ui.ViewModels;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls;
using sdmx_dl_ui.Models;
using Splat;
using Splat.NLog;

namespace sdmx_dl_ui.Views
{
    public partial class MainView
    {
        static MainView()
        {
            Locator.CurrentMutable.Register( () => new FlowDropDownView() , typeof( IViewFor<FlowViewModel> ) , "FlowDropDown" );
            Locator.CurrentMutable.Register( () => new DimensionSelectorView() , typeof( IViewFor<DimensionSelectorViewModel> ) );
            Locator.CurrentMutable.Register( () => new CodeSelectorView() , typeof( IViewFor<CodeSelectorViewModel> ) );

            Locator.CurrentMutable.Register( () => new SeriesListView() , typeof( IViewFor<SeriesViewModel> ) , "SeriesList" );
            Locator.CurrentMutable.Register( () => new BrowserView() , typeof( IViewFor<BrowserViewModel> ) );

            Locator.CurrentMutable.UseNLogWithWrappingFullLogger();
        }

        public MainView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .BindTo( this , x => x.DataContext )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( MainView view , ScriptsViewModel viewModel , CompositeDisposable disposables )
        {
            view.OneWayBind( viewModel ,
                vm => vm.IsMissingTool ,
                v => v.MissingMessageBorder.Visibility ,
                b => b ? Visibility.Visible : Visibility.Collapsed )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.IsWorking ,
                    v => v.WorkingBorder.Visibility ,
                    b => b ? Visibility.Visible : Visibility.Collapsed )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.IsWorking ,
                    v => v.WorkingContentBorder.Visibility ,
                    b => b ? Visibility.Visible : Visibility.Collapsed )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                vm => vm.Sources ,
                v => v.ComboBoxSources.ItemsSource )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                vm => vm.SelectedSource ,
                v => v.ComboBoxSources.SelectedItem )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.Flows ,
                    v => v.ComboBoxFlows.ItemsSource )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                    vm => vm.SelectedFlow ,
                    v => v.ComboBoxFlows.SelectedItem )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                    vm => vm.FlowText ,
                    v => v.ComboBoxFlows.Text )
                .DisposeWith( disposables );

            view.ComboBoxFlows.Events().GotKeyboardFocus.Select( _ => Unit.Default ).Merge(
                view.ComboBoxFlows.Events().PreviewTextInput.Select( _ => Unit.Default ) )
                .SubscribeSafe( _ => { view.ComboBoxFlows.IsDropDownOpen = true; } )
                .DisposeWith( disposables );

            view.ComboBoxFlows.Events().KeyUp
                .Where( x => x.Key == Key.Escape )
                .SubscribeSafe( _ => viewModel.FlowText = string.Empty )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.DimensionsSelectors ,
                    v => v.ItemsControlDimensionsSelectors.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.ResultingKey ,
                    v => v.TextBlockKey.Text )
                .DisposeWith( disposables );

            view.Bind( viewModel ,
                    vm => vm.FullExport ,
                    v => v.CheckBoxFullExport.IsChecked )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                    vm => vm.CopyToClipboardCommand ,
                    v => v.ButtonCopy )
                .DisposeWith( disposables );

            view.BindCommand( viewModel ,
                    vm => vm.OpenInBrowserCommand ,
                    v => v.ButtonBrowse )
                .DisposeWith( disposables );

            view.Snackbar.MessageQueue = viewModel.MessageQueue;

            view.ComboBoxSources.Focus();

            viewModel.OpenInBrowserInteraction.RegisterHandler( ctx =>
            {
                var model = new BrowserViewModel( viewModel ) { SeriesIdentifier = ctx.Input };
                var window = new MetroWindow
                {
                    Owner = Window.GetWindow( view ) ,
                    Title = ctx.Input ,
                    Content = new ViewModelViewHost
                    {
                        ViewModel = model ,
                        HorizontalAlignment = HorizontalAlignment.Stretch ,
                        HorizontalContentAlignment = HorizontalAlignment.Stretch ,
                        VerticalAlignment = VerticalAlignment.Stretch ,
                        VerticalContentAlignment = VerticalAlignment.Stretch
                    } ,
                    Style = (Style) view.TryFindResource( "MainWindowStyle" )
                };
                window.Show();

                ctx.SetOutput( Unit.Default );
            } );
        }
    }
}