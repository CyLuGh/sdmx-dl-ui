﻿using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReactiveUI;
using sdmx_dl_ui.ViewModels;

namespace sdmx_dl_ui.Views
{
    /// <summary>
    /// Interaction logic for FlowDropDownView.xaml
    /// </summary>
    public partial class FlowDropDownView
    {
        public FlowDropDownView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private void PopulateFromViewModel( FlowDropDownView view , FlowViewModel viewModel , CompositeDisposable disposables )
        {
            view.TextBlockRef.Text = viewModel.Ref;
            view.TextBlockLabel.Text = viewModel.Label;
        }
    }
}
