﻿using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Media;
using ReactiveUI;
using sdmx_dl_ui.Models;
using sdmx_dl_ui.ViewModels;
using sdmx_dl_ui.Views.Infrastructure;

namespace sdmx_dl_ui.Views
{
    /// <summary>
    /// Interaction logic for SeriesListView.xaml
    /// </summary>
    public partial class SeriesListView
    {
        public SeriesListView()
        {
            InitializeComponent();
            PlotViewSpark.Configure( false , true );

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }

        private static void PopulateFromViewModel( SeriesListView view , SeriesViewModel viewModel , CompositeDisposable disposables )
        {
            view.TextBlockName.Text = viewModel.Name;

            view.OneWayBind( viewModel ,
                    vm => vm.Data.Count ,
                    v => v.TextBlockObservations.Text ,
                    cnt => cnt > 1 ? $"{cnt} observations" : $"{cnt} observation" )
                .DisposeWith( disposables );

            viewModel.DrawSparkLineInteraction.RegisterHandler( ctx =>
                 {
                     var brush = (Brush) view.PlotViewSpark.TryFindResource( "PrimaryHueMidBrush" ) ?? Brushes.DarkGray;

                     view.PlotViewSpark.Model.Series.Clear();
                     view.PlotViewSpark.Model.AddLineSeries( ctx.Input , brush );
                     view.PlotViewSpark.InvalidatePlot();
                     ctx.SetOutput( Unit.Default );
                 } )
                .DisposeWith( disposables );
        }
    }
}
