﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace sdmx_dl_ui.Views
{
    public partial class HourglassProgress : UserControl
    {
        public HourglassProgress()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty StrokeUpperProperty = DependencyProperty.Register(
            "StrokeUpper" , typeof( Brush ) , typeof( HourglassProgress ) , new PropertyMetadata( default( Brush ) ) );

        public Brush StrokeUpper
        {
            get => (Brush) GetValue( StrokeUpperProperty );
            set => SetValue( StrokeUpperProperty , value );
        }

        public static readonly DependencyProperty StrokeLowerProperty = DependencyProperty.Register(
            "StrokeLower" , typeof( Brush ) , typeof( HourglassProgress ) , new PropertyMetadata( default( Brush ) ) );

        public Brush StrokeLower
        {
            get => (Brush) GetValue( StrokeLowerProperty );
            set => SetValue( StrokeLowerProperty , value );
        }
    }
}
