﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using DynamicData;
using ReactiveUI;
using sdmx_dl_ui.Models;
using sdmx_dl_ui.ViewModels;
using sdmx_dl_ui.Views.Infrastructure;

namespace sdmx_dl_ui.Views
{
    public partial class BrowserView
    {
        private static List<SolidColorBrush> Brushes { get; }
        static BrowserView()
        {
            Brushes = new List<SolidColorBrush>
            {
                new SolidColorBrush(Color.FromRgb(245, 121, 0)),
                new SolidColorBrush(Color.FromRgb(51, 101, 164)),
                new SolidColorBrush(Color.FromRgb(115, 210, 21)),
                new SolidColorBrush(Color.FromRgb(117, 79, 123)),
                new SolidColorBrush(Color.FromRgb(193, 125, 16)),
                new SolidColorBrush(Color.FromRgb(164, 0, 0)),
                new SolidColorBrush(Color.FromRgb(196, 160, 0))
            };
        }

        public BrowserView()
        {
            InitializeComponent();

            this.WhenActivated( disposables =>
            {
                this.WhenAnyValue( x => x.ViewModel )
                    .WhereNotNull()
                    .Do( vm => PopulateFromViewModel( this , vm , disposables ) )
                    .SubscribeSafe()
                    .DisposeWith( disposables );
            } );
        }



        private static void PopulateFromViewModel( BrowserView view , BrowserViewModel viewModel , CompositeDisposable disposables )
        {
            view.ListBoxSeries.ItemsSource = viewModel.Series;
            view.PlotViewSeries.Configure();

            view.ListBoxSeries.Events().SelectionChanged
                .Subscribe( args =>
                {
                    viewModel.SelectedSeriesCache.Remove( args.RemovedItems.OfType<SeriesViewModel>() );
                    viewModel.SelectedSeriesCache.AddOrUpdate( args.AddedItems.OfType<SeriesViewModel>().Where( x => x.Data != null ) );
                } )
                .DisposeWith( disposables );

            viewModel.CreateSelectedSeriesColumnsInteraction
                .RegisterHandler( ctx =>
                {
                    view.DataGridSelection.BuildColumns( ctx.Input );
                    ctx.SetOutput( Unit.Default );
                } )
                .DisposeWith( disposables );

            viewModel.DrawSelectedSeriesInteraction
                .RegisterHandler( ctx =>
                {
                    view.PlotViewSeries.Model.Series.Clear();

                    foreach ( var series in ctx.Input )
                    {
                        var cs = new ChartSeries
                        {
                            Title = series.Name ,
                            DataSource = series.Data.ToDictionary( x => x.period , x => x.value )
                        };

                        var idx = view.PlotViewSeries.Model.Series.Count;
                        while ( idx >= Brushes.Count )
                            idx -= Brushes.Count;

                        view.PlotViewSeries.Model.AddLineSeries( cs , Brushes[idx] );
                    }


                    view.PlotViewSeries.InvalidatePlot();
                    ctx.SetOutput( Unit.Default );
                } )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.Data ,
                    v => v.DataGridSelection.ItemsSource )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.IsWorking ,
                    v => v.WorkingBorder.Visibility ,
                    b => b ? Visibility.Visible : Visibility.Collapsed )
                .DisposeWith( disposables );

            view.OneWayBind( viewModel ,
                    vm => vm.IsWorking ,
                    v => v.WorkingContentBorder.Visibility ,
                    b => b ? Visibility.Visible : Visibility.Collapsed )
                .DisposeWith( disposables );
        }
    }
}
