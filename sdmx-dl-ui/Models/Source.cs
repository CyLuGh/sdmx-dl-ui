﻿namespace sdmx_dl_ui.Models
{
    public class Source
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Aliases { get; set; }

        public override string ToString()
            => $"{Name} - {Description}";
    }
}
