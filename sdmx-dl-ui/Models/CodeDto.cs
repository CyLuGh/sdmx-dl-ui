﻿namespace sdmx_dl_ui.Models
{
    public class CodeDto
    {
        public string Code { get; set; }
        public string Label { get; set; }
    }
}
