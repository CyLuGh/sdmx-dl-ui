﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sdmx_dl_ui.Models
{
    public class SdmxKey
    {
        public string Key { get; set; }
        public string Freq { get; set; }

        public override string ToString()
            => $"{Key}";
    }
}
