﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sdmx_dl_ui.Models
{
    public class Dimension
    {
        public string Concept { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public int Position { get; set; }

        public override string ToString()
            => $"{Position} - {Concept}";
    }
}
