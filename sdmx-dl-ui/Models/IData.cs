﻿using System;

namespace sdmx_dl_ui.Models
{
    public interface IData
    {
        DateTime Period { get; set; }
    }
}
