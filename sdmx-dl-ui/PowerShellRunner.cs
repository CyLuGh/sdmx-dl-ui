﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management.Automation;
using CsvHelper;

namespace sdmx_dl_ui
{
    internal static class PowerShellRunner
    {
        public static T[] Query<T>( params string[] arguments )
        {
            var query = PowerShell.Create()
                .AddCommand( "sdmx-dl" );

            foreach ( var argument in arguments )
                query.AddArgument( argument );

            var res = query.Invoke()
                .Select( p => p.ToString() )
                .ToArray();
            var strOutput = string.Join( Environment.NewLine , res );

            var list = new List<T>();
            using ( TextReader textReader = new StringReader( strOutput ) )
            {
                using var csvReader = new CsvReader( textReader , CultureInfo.InvariantCulture );
                while ( csvReader.Read() )
                    list.Add( csvReader.GetRecord<T>() );
            }
            return list.ToArray();
        }

        public static string[] Query( params string[] arguments )
        {
            var query = PowerShell.Create()
                .AddCommand( "sdmx-dl" );

            foreach ( var argument in arguments )
                query.AddArgument( argument );

            return query.Invoke()
                .Select( p => p.ToString() )
                .ToArray();
        }
    }
}
