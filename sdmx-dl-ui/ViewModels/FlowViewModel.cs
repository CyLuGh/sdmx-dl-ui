﻿using System;
using System.Collections.Generic;
using System.Text;
using ReactiveUI;

namespace sdmx_dl_ui.ViewModels
{
    public class FlowViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public string Ref { get; set; }
        public string Label { get; set; }

        public override string ToString()
            => $"{Ref} - {Label}";

        public FlowViewModel()
        {
            Activator = new ViewModelActivator();
        }
    }
}
