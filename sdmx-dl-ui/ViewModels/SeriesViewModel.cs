﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using sdmx_dl_ui.Models;

namespace sdmx_dl_ui.ViewModels
{
    public class SeriesViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public string Name { get; }
        public string MappedName { get; }

        private SourceCache<(DateTime period, double value) , DateTime> DataCache { get; }
            = new SourceCache<(DateTime period, double value) , DateTime>( x => x.period );

        private ReadOnlyObservableCollection<(DateTime period, double value)> _data;
        public ReadOnlyObservableCollection<(DateTime period, double value)> Data => _data;

        public Interaction<ChartSeries , Unit> DrawSparkLineInteraction { get; }
            = new Interaction<ChartSeries , Unit>( RxApp.MainThreadScheduler );


        private SeriesViewModel()
        {
            Activator = new ViewModelActivator();

            DrawSparkLineInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );

            this.WhenActivated( disposables =>
            {
                DataCache.Connect()
                    .Filter( t => !double.IsNaN( t.value ) )
                    .Sort( SortExpressionComparer<(DateTime period, double value)>.Ascending( x => x.period ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _data )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);

                Data.ObserveCollectionChanges()
                    .ObserveOn(RxApp.TaskpoolScheduler)
                    .Throttle( TimeSpan.FromMilliseconds( 200 ) )
                    .SubscribeSafe( async _ =>
                    {
                        var cs = new ChartSeries
                        {
                            Title = Name ,
                            DataSource = Data.ToDictionary( x => x.period , x => x.value )
                        };
                        await DrawSparkLineInteraction.Handle( cs );
                    } )
                    .DisposeWith( disposables );
            } );
        }

        public SeriesViewModel( string name , string mappedName , IEnumerable<(DateTime, double)> data ) : this()
        {
            Name = name;
            MappedName = mappedName;
            DataCache.AddOrUpdate( data );
        }
    }
}
