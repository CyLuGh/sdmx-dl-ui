﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using sdmx_dl_ui.Models;

namespace sdmx_dl_ui.ViewModels
{
    public class DimensionSelectorViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public string Concept { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public int Position { get; set; }

        private SourceCache<CodeSelectorViewModel , string> CodesCache { get; }
            = new SourceCache<CodeSelectorViewModel , string>( c => c.Code );

        private ReadOnlyObservableCollection<CodeSelectorViewModel> _codes;
        public ReadOnlyObservableCollection<CodeSelectorViewModel> Codes => _codes;

        [Reactive] public string KeyPart { get; set; }
        [Reactive] public string SearchText { get; set; }
        [Reactive] public bool IsSingleton { get; set; }

        public bool HasSelection { [ObservableAsProperty] get; }

        public ReactiveCommand<Unit , Unit> ClearSelectionCommand { get; private set; }

        public DimensionSelectorViewModel()
        {
            Activator = new ViewModelActivator();

            InitializeCommands( this );

            var filter = this.WhenAnyValue( x => x.SearchText )
                .Throttle( TimeSpan.FromMilliseconds( 300 ) )
                .ObserveOn( RxApp.TaskpoolScheduler )
                .Select( BuildFilter );

            this.WhenActivated( disposables =>
            {
                CodesCache.Connect()
                    .ObserveOn( RxApp.TaskpoolScheduler )
                    .Filter( filter )
                    .Sort( SortExpressionComparer<CodeSelectorViewModel>.Ascending( x => x.Label ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _codes )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith( disposables );

                CodesCache.Connect()
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .SubscribeSafe( _ =>
                    {
                        IsSingleton = ( CodesCache.Items.Count() == 1 );
                    } )
                    .DisposeWith( disposables );

                CodesCache.Connect()
                    .AutoRefresh( x => x.IsSelected )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Do( _ =>
                    {
                        KeyPart =
                            string.Join( "+" ,
                                CodesCache.Items
                                    .Where( c => c.IsSelected )
                                    .Select( c => c.Code ) );
                    } )
                    .Select( _ => CodesCache.Items.Any( x => x.IsSelected ) )
                    .ToPropertyEx( this , x => x.HasSelection , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );
            } );
        }

        private static void InitializeCommands( DimensionSelectorViewModel @this )
        {
            @this.ClearSelectionCommand = ReactiveCommand.Create( () =>
            {
                foreach ( var codeSelectorViewModel in @this.CodesCache.Items )
                {
                    codeSelectorViewModel.IsSelected = false;
                }
            } );
        }

        public void Add( IEnumerable<CodeDto> codes )
        {
            CodesCache.Clear();

            var codeDtos = codes as CodeDto[] ?? codes.ToArray();
            var isSingleton = codeDtos.Count() == 1;

            CodesCache.AddOrUpdate( codeDtos.Select( c => new CodeSelectorViewModel
            {
                Code = c.Code ,
                Label = c.Label ,
                IsSelected = isSingleton
            } ) );
        }

        private Func<CodeSelectorViewModel , bool> BuildFilter( string searchText )
        {
            if ( string.IsNullOrEmpty( searchText ) )
                return _ => true;

            return f => f.Code.Contains( searchText , StringComparison.OrdinalIgnoreCase )
                        || f.Label.Contains( searchText , StringComparison.OrdinalIgnoreCase )
                        || string.CompareOrdinal( searchText , f.ToString() ) == 0;
        }
    }
}
