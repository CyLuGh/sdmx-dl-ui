﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using CsvHelper;
using DynamicData;
using DynamicData.Binding;
using Namotion.Reflection;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using sdmx_dl_ui.Models;
using Splat;

namespace sdmx_dl_ui.ViewModels
{
    public class BrowserViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        [Reactive] public string SeriesIdentifier { get; set; }
        [Reactive] public IDictionary<string , string> PropertiesMapping { get; set; }
        [Reactive] public Type DataType { get; set; }

        private SourceCache<IData , DateTime> DataCache { get; }
            = new SourceCache<IData , DateTime>( x => x.Period );

        private ReadOnlyObservableCollection<IData> _data;
        public ReadOnlyObservableCollection<IData> Data => _data;

        private SourceCache<SeriesViewModel , string> SeriesCache { get; }
            = new SourceCache<SeriesViewModel , string>( x => x.Name );

        private ReadOnlyObservableCollection<SeriesViewModel> _series;
        public ReadOnlyObservableCollection<SeriesViewModel> Series => _series;

        internal SourceCache<SeriesViewModel , string> SelectedSeriesCache { get; }
            = new SourceCache<SeriesViewModel , string>( x => x.Name );

        public bool IsWorking { [ObservableAsProperty] get; }

        private ReadOnlyObservableCollection<SeriesViewModel> _selectedSeries;
        public ReadOnlyObservableCollection<SeriesViewModel> SelectedSeries => _selectedSeries;

        public ReactiveCommand<string , Unit> RetrieveDataCommand { get; private set; }

        public Interaction<IEnumerable<SeriesViewModel> , Unit> DrawSelectedSeriesInteraction { get; }
            = new Interaction<IEnumerable<SeriesViewModel> , Unit>( RxApp.MainThreadScheduler );
        public Interaction<IEnumerable<SeriesViewModel> , Unit> CreateSelectedSeriesColumnsInteraction { get; }
            = new Interaction<IEnumerable<SeriesViewModel> , Unit>( RxApp.MainThreadScheduler );

        public BrowserViewModel( ScriptsViewModel svm )
        {
            Activator = new ViewModelActivator();

            CreateSelectedSeriesColumnsInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );
            DrawSelectedSeriesInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );

            InitializeCommands( this );

            this.WhenActivated( disposables =>
            {
                SeriesCache.Connect()
                    .Sort( SortExpressionComparer<SeriesViewModel>.Ascending( x => x.Name ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _series )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith( disposables );

                SelectedSeriesCache.Connect()
                    .Sort( SortExpressionComparer<SeriesViewModel>.Ascending( x => x.Name ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _selectedSeries )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith( disposables );

                DataCache.Connect()
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _data )
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SeriesIdentifier )
                    .InvokeCommand( RetrieveDataCommand );

                SelectedSeries.ObserveCollectionChanges()
                    .Throttle( TimeSpan.FromMilliseconds( 300 ) )
                    .SubscribeSafe( async _ =>
                    {
                        await CreateSelectedSeriesColumnsInteraction.Handle( SelectedSeries );
                        await DrawSelectedSeriesInteraction.Handle( SelectedSeries );
                    } )
                    .DisposeWith( disposables );

                RetrieveDataCommand.IsExecuting
                    .ToPropertyEx( this , x => x.IsWorking , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                RetrieveDataCommand.ThrownExceptions.Select( ex => ("Couln't retrieve sources", ex) )
                    .SubscribeSafe( x =>
                    {
                        this.Log().Error( "{0}: {1}" , x.Item1 , x.ex.Message );
                        svm.MessageQueue.Enqueue( $"{x.Item1}: {x.ex.Message}" );
                    } )
                    .DisposeWith( disposables );

            } );
        }

        private static void InitializeCommands( BrowserViewModel @this )
        {
            @this.RetrieveDataCommand = ReactiveCommand.CreateFromObservable<string , Unit>( identifier =>
                  Observable.Start( () =>
                  {
                      if ( string.IsNullOrWhiteSpace( identifier ) ) return;

                      var parameters = identifier.Split( ' ' ).Prepend( "data" ).ToArray();
                      var data = PowerShellRunner.Query( parameters );

                      var meta = data.First().Split( ',' );

                      var properties = meta
                          .Skip( 1 )
                          .ToDictionary( s => s , _ => Guid.NewGuid().ToString( "N" ).ToUpper() );

                      @this.DataType =
                          ReflectionTypeGenerator.GenerateTypedObject( properties.Select( x =>
                                (x.Value, typeof( double )) ) , interfaces: new[] { typeof( IData ) } );
                      @this.PropertiesMapping = properties;

                      var strOutput = string.Join( Environment.NewLine , data );

                      var res = new List<IData>();

                      using TextReader textReader = new StringReader( strOutput );
                      using var csvReader = new CsvReader( textReader , CultureInfo.InvariantCulture );
                      csvReader.Read();
                      while ( csvReader.Read() )
                      {
                          var idata = (IData) System.Activator.CreateInstance( @this.DataType );

                          idata.Period = csvReader.GetField<DateTime>( 0 );

                          for ( int i = 1 ; i < meta.Length ; i++ )
                          {
                              var metaProperty = meta[i];
                              var src = csvReader.GetField<double?>( i );

                              if ( properties.TryGetValue( metaProperty , out var mapping ) )
                              {
                                  @this.DataType.InvokeMember( mapping , System.Reflection.BindingFlags.SetProperty , null ,
                                      idata , src != null ? new object[] { src.Value } : new object[] { double.NaN } );
                              }
                          }

                          res.Add( idata );
                      }

                      @this.DataCache.AddOrUpdate( res );

                      meta.Skip( 1 )
                          .AsParallel()
                          .ForAll( name => @this.SeriesCache.AddOrUpdate( BuildSeries( res , name , properties ) ) );
                  } ) );
        }

        private static SeriesViewModel BuildSeries( List<IData> raws , string name , IDictionary<string , string> mappings )
        {
            var mappedName = mappings.TryGetValue( name , out var mapped ) ? mapped : name;
            return new SeriesViewModel( name ,
                mappedName ,
                raws.Select( x => (x.Period,
                     x.TryGetPropertyValue( mappedName , double.NaN )) ) );
        }
    }


}
