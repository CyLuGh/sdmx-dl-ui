﻿using DynamicData;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Threading;
using MaterialDesignThemes.Wpf;
using sdmx_dl_ui.Models;
using sdmx_dl_ui.Views.Infrastructure;
using Splat;

namespace sdmx_dl_ui.ViewModels
{
    public class ScriptsViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public KeyDragHandler KeyDragHandler { get; }

        public bool IsMissingTool { [ObservableAsProperty] get; }
        public bool IsWorking { [ObservableAsProperty] get; }
        public string ResultingKey { [ObservableAsProperty] get; }
        public Source[] Sources { [ObservableAsProperty]get; }

        private SourceCache<FlowViewModel , string> FlowsCache { get; }
            = new SourceCache<FlowViewModel , string>( f => f.Ref );

        private ReadOnlyObservableCollection<FlowViewModel> _flows;
        public ReadOnlyObservableCollection<FlowViewModel> Flows => _flows;

        private SourceCache<DimensionSelectorViewModel , string> DimensionsSelectorsCache { get; }
            = new SourceCache<DimensionSelectorViewModel , string>( ds => ds.Concept );

        private ReadOnlyObservableCollection<DimensionSelectorViewModel> _dimensionsSelectors;
        public ReadOnlyObservableCollection<DimensionSelectorViewModel> DimensionsSelectors => _dimensionsSelectors;

        internal SnackbarMessageQueue MessageQueue { get; }
            = new SnackbarMessageQueue( TimeSpan.FromSeconds( 20 ) , Dispatcher.CurrentDispatcher );

        [Reactive] public bool FullExport { get; set; } = true;
        [Reactive] public Source SelectedSource { get; set; }
        [Reactive] public FlowViewModel SelectedFlow { get; set; }
        [Reactive] public string FlowText { get; set; }
        [Reactive] public SdmxKey[] Keys { get; set; }
        [Reactive] public Feature[] Features { get; set; }
        [Reactive] public Dimension[] Dimensions { get; set; }
        [Reactive] public IDictionary<string , CodeDto[]> Codes { get; set; }

        public ReactiveCommand<Unit , Unit> TestToolCommand { get; private set; }
        public ReactiveCommand<Unit , Source[]> RetrieveSourcesCommand { get; private set; }
        public ReactiveCommand<Source , FlowViewModel[]> RetrieveFlowsCommand { get; private set; }
        public ReactiveCommand<(Source, FlowViewModel) , SdmxKey[]> RetrieveKeysCommand { get; private set; }
        public ReactiveCommand<(Source, FlowViewModel) , Dimension[]> RetrieveDimensionsCommand { get; private set; }
        public ReactiveCommand<(Source, FlowViewModel, Dimension[]) , IDictionary<string , CodeDto[]>> RetrieveCodesCommand { get; private set; }
        public ReactiveCommand<Source , Feature[]> RetrieveFeaturesCommand { get; private set; }
        public ReactiveCommand<(Dimension[], IDictionary<string , CodeDto[]>, Feature[], SdmxKey[]) , Unit> BuildDimensionsSelectors { get; private set; }
        public ReactiveCommand<Unit , Unit> CopyToClipboardCommand { get; private set; }
        public ReactiveCommand<Unit , Unit> OpenInBrowserCommand { get; private set; }

        public Interaction<string , Unit> OpenInBrowserInteraction { get; }
            = new Interaction<string , Unit>( RxApp.MainThreadScheduler );


        public ScriptsViewModel()
        {
            Activator = new ViewModelActivator();
            KeyDragHandler = new KeyDragHandler( this );

            OpenInBrowserInteraction.RegisterHandler( ctx => ctx.SetOutput( Unit.Default ) );
            InitializeCommands( this );

            this.WhenActivated( disposables =>
            {
                TestToolCommand
                    .Select( _ => false )
                    .ToPropertyEx( this , x => x.IsMissingTool , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                TestToolCommand
                    .InvokeCommand( RetrieveSourcesCommand )
                    .DisposeWith( disposables );

                TestToolCommand
                    .ThrownExceptions
                    .Do( ex => this.Log().Error( ex ) )
                    .Select( _ => true )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .ToPropertyEx( this , x => x.IsMissingTool , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                RetrieveSourcesCommand
                    .ToPropertyEx( this , x => x.Sources , scheduler: RxApp.MainThreadScheduler )
                    .DisposeWith( disposables );

                RetrieveFlowsCommand
                    .SubscribeSafe( flows =>
                    {
                        FlowsCache.Clear();
                        FlowsCache.AddOrUpdate( flows );
                    } )
                    .DisposeWith( disposables );

                RetrieveKeysCommand
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .SubscribeSafe( keys => Keys = keys )
                    .DisposeWith( disposables );

                RetrieveDimensionsCommand
                    .Select( dimensions => (SelectedSource, SelectedFlow, dimensions) )
                    .InvokeCommand( RetrieveCodesCommand )
                    .DisposeWith( disposables );

                RetrieveDimensionsCommand
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .SubscribeSafe( dimensions => Dimensions = dimensions )
                    .DisposeWith( disposables );

                RetrieveFeaturesCommand
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .SubscribeSafe( features => Features = features )
                    .DisposeWith( disposables );

                RetrieveCodesCommand
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .SubscribeSafe( codes => Codes = codes )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SelectedSource )
                    .Do( _ =>
                    {
                        SelectedFlow = null;
                        Features = null;
                        FlowText = string.Empty;
                    } )
                    .WhereNotNull()
                    .InvokeCommand( RetrieveFlowsCommand )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SelectedSource )
                    .WhereNotNull()
                    .InvokeCommand( RetrieveFeaturesCommand )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.FlowText )
                    .Where( string.IsNullOrEmpty )
                    .SubscribeSafe( _ => SelectedFlow = null )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SelectedFlow )
                    .SubscribeSafe( _ => Clear() )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SelectedSource )
                    .CombineLatest( this.WhenAnyValue( x => x.SelectedFlow ).WhereNotNull() ,
                        ( src , flw ) => (src, flw) )
                    .Throttle( TimeSpan.FromMilliseconds( 250 ) )
                    .InvokeCommand( RetrieveKeysCommand )
                    .DisposeWith( disposables );

                this.WhenAnyValue( x => x.SelectedSource )
                    .CombineLatest( this.WhenAnyValue( x => x.SelectedFlow ).WhereNotNull() ,
                        ( src , flw ) => (src, flw) )
                    .Throttle( TimeSpan.FromMilliseconds( 250 ) )
                    .InvokeCommand( RetrieveDimensionsCommand )
                    .DisposeWith( disposables );

                Observable.CombineLatest(
                    this.WhenAnyValue( x => x.Dimensions ) ,
                    this.WhenAnyValue( x => x.Codes ) ,
                    this.WhenAnyValue( x => x.Features ) ,
                    this.WhenAnyValue( x => x.Keys ) ,
                    ( dimensions , codes , features , keys )
                        => (dimensions, codes, features, keys) )
                    .Throttle( TimeSpan.FromMilliseconds( 250 ) )
                    .InvokeCommand( BuildDimensionsSelectors )
                    .DisposeWith( disposables );

                RetrieveSourcesCommand.IsExecuting.CombineLatest(
                        RetrieveFlowsCommand.IsExecuting ,
                        RetrieveKeysCommand.IsExecuting ,
                        RetrieveDimensionsCommand.IsExecuting ,
                        RetrieveFeaturesCommand.IsExecuting ,
                        RetrieveCodesCommand.IsExecuting ,
                        BuildDimensionsSelectors.IsExecuting ,
                        ( a , b , c , d , e , f , g ) => new[] { a , b , c , d , e , f , g }.Any( x => x ) )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .ToPropertyEx( this , x => x.IsWorking )
                    .DisposeWith( disposables );

                var filter = this.WhenAnyValue( x => x.FlowText )
                    .Throttle( TimeSpan.FromMilliseconds( 250 ) )
                    .Select( BuildFilter );

                FlowsCache.Connect()
                    .Filter( filter )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _flows )
                    .DisposeMany()
                    .SubscribeSafe()
                    .DisposeWith( disposables );

                DimensionsSelectorsCache.Connect()
                    .AutoRefresh( x => x.IsSingleton )
                    .Filter( x => x.IsSingleton == false )
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Bind( out _dimensionsSelectors )
                    .DisposeMany()
                    .SubscribeSafe()
                    .DisposeWith( disposables );

                DimensionsSelectorsCache.Connect()
                    .AutoRefresh( x => x.KeyPart )
                    .Throttle( TimeSpan.FromMilliseconds( 200 ) )
                    .Select( _ =>
                         string.Join( "." ,
                             DimensionsSelectorsCache.Items.OrderBy( x => x.Position ).Select( o => o.KeyPart ) ) )
                    .ToPropertyEx( this , x => x.ResultingKey , scheduler: RxApp.MainThreadScheduler );

                Observable.Merge( RetrieveSourcesCommand.ThrownExceptions.Select( ex => ("Couln't retrieve sources", ex) ) ,
                        RetrieveFlowsCommand.ThrownExceptions.Select( ex => ("Couln't retrieve flows", ex) ) ,
                        RetrieveFlowsCommand.ThrownExceptions.Select( ex => ("Couln't retrieve flows", ex) ) ,
                        RetrieveKeysCommand.ThrownExceptions.Select( ex => ("Couln't retrieve keys", ex) ) ,
                        RetrieveDimensionsCommand.ThrownExceptions.Select( ex => ("Couln't retrieve dimensions", ex) ) ,
                        RetrieveCodesCommand.ThrownExceptions.Select( ex => ("Couln't retrieve codes", ex) ) ,
                        RetrieveFeaturesCommand.ThrownExceptions.Select( ex => ("Couln't retrieve features", ex) ) ,
                        BuildDimensionsSelectors.ThrownExceptions.Select( ex => ("Couldn't build dimensions selection", ex) ) ,
                        CopyToClipboardCommand.ThrownExceptions.Select( ex => ("Couldn't copy to clipboard", ex) ) )
                    .SubscribeSafe( x =>
                    {
                        this.Log().Error( "{0}: {1}" , x.Item1 , x.ex.Message );
                        MessageQueue.Enqueue( $"{x.Item1}: {x.ex.Message}" );
                    } )
                    .DisposeWith( disposables );

                Observable.Timer( TimeSpan.FromMinutes( 2 ) , TimeSpan.FromMinutes( 2 ) )
                    .SubscribeSafe( _ =>
                    {
                        this.Log().Debug( "Calling GC" );
                        GC.Collect();
                    } )
                    .DisposeWith( disposables );

                Observable.Return( Unit.Default )
                     .InvokeCommand( TestToolCommand )
                     .DisposeWith( disposables );
            } );
        }

        private void Clear()
        {
            DimensionsSelectorsCache.Clear();
            Dimensions = null;
            Keys = null;
            Codes = null;
        }

        private static void InitializeCommands( ScriptsViewModel @this )
        {
            @this.TestToolCommand = ReactiveCommand.CreateFromObservable( () =>
                 Observable.Start( () =>
                 {
                     PowerShell.Create()
                         .AddCommand( "sdmx-dl" )
                         .Invoke();
                 } ) );

            @this.RetrieveSourcesCommand = ReactiveCommand.CreateFromObservable( () =>
                 Observable.Start( () => PowerShellRunner.Query<Source>( "list" , "sources" ) ) );

            @this.RetrieveFlowsCommand = ReactiveCommand.CreateFromObservable<Source , FlowViewModel[]>( source =>
                   Observable.Start( () => PowerShellRunner.Query<FlowViewModel>( "list" , "flows" , source.Name ) ) );

            var canRetrieveKeysAndDimensions = @this.WhenAnyValue( x => x.SelectedSource )
                .CombineLatest( @this.WhenAnyValue( x => x.SelectedFlow ) ,
                    ( src , flw ) => src != null && flw != null );

            @this.RetrieveKeysCommand = ReactiveCommand.CreateFromObservable<(Source, FlowViewModel) , SdmxKey[]>( input =>
                  Observable.Start( () =>
                  {
                      var (source, flow) = input;
                      return PowerShellRunner.Query<SdmxKey>( "list" , "keys" , source.Name , flow.Ref );
                  } ) , canRetrieveKeysAndDimensions );

            @this.RetrieveDimensionsCommand = ReactiveCommand.CreateFromObservable<(Source, FlowViewModel) , Dimension[]>( input =>
                Observable.Start( () =>
                {
                    var (source, flow) = input;
                    return PowerShellRunner.Query<Dimension>( "list" , "concepts" , source.Name , flow.Ref );
                } ) , canRetrieveKeysAndDimensions );

            @this.RetrieveCodesCommand =
                ReactiveCommand
                    .CreateFromObservable<(Source, FlowViewModel, Dimension[]) , IDictionary<string , CodeDto[]>>(
                        input =>
                            Observable.Start( () =>
                            {
                                var (source, flow, dimensions) = input;

                                var codes = new ConcurrentDictionary<string , CodeDto[]>();

                                dimensions.AsParallel()
                                    .ForAll( dimension =>
                                    {
                                        var dimCodes = PowerShellRunner.Query<CodeDto>( "list" , "codes" , source.Name , flow.Ref ,
                                            dimension.Concept );
                                        codes.TryAdd( dimension.Concept , dimCodes );
                                    } );

                                return codes;
                            } ) );

            @this.RetrieveFeaturesCommand = ReactiveCommand.CreateFromObservable<Source , Feature[]>( source =>
                  Observable.Start( () => PowerShellRunner.Query<Feature>( "list" , "features" , source.Name ) ) );

            var canBuildDimensionsSelector = @this.WhenAnyValue( x => x.Dimensions )
                .CombineLatest( @this.WhenAnyValue( x => x.Codes ) ,
                    @this.WhenAnyValue( x => x.Features ) ,
                    @this.WhenAnyValue( x => x.Keys ) ,
                    ( dimensions , codes , features , keys ) => dimensions != null
                                                           && codes != null
                                                           && features != null
                                                           && keys != null );


            @this.BuildDimensionsSelectors = ReactiveCommand
                .CreateFromObservable<(Dimension[], IDictionary<string , CodeDto[]>, Feature[], SdmxKey[]) , Unit>(
                    input =>
                        Observable.Start( () =>
                        {
                            var (concepts, codes, features, keys) = input;

                            var filter = features.Any( x => x != null && x.SupportedFeature.Equals( "SERIES_KEYS_ONLY" ) );
                            IDictionary<int , HashSet<string>> allowedValues = null;
                            if ( filter && keys.Any() )
                            {
                                var splittedKeys = keys.Select( x => x.Key.Split( '.' ) )
                                    .ToArray();

                                allowedValues = concepts.ToDictionary( x => x.Position ,
                                    x => new HashSet<string>( splittedKeys.Select( s => s[x.Position - 1] ).Distinct() ) );
                            }

                            @this.DimensionsSelectorsCache.Clear();
                            @this.DimensionsSelectorsCache.AddOrUpdate(
                                concepts.Select( concept =>
                                {
                                    var dsvm = new DimensionSelectorViewModel
                                    {
                                        Label = concept.Label ,
                                        Concept = concept.Concept ,
                                        Position = concept.Position ,
                                        Type = concept.Type
                                    };

                                    if ( codes.TryGetValue( concept.Concept , out var codeList ) )
                                    {
                                        dsvm.Add( codeList.Where( c => allowedValues == null
                                                                  || ( allowedValues.TryGetValue( concept.Position , out var allowed )
                                                                        && allowed.Contains( c.Code ) ) ) );
                                    }

                                    return dsvm;
                                } ) );
                        } ) , canBuildDimensionsSelector );

            var canCopy =
                @this.WhenAnyValue( x => x.SelectedSource )
                    .CombineLatest( @this.WhenAnyValue( x => x.SelectedFlow ) ,
                        ( src , flw ) => src != null && flw != null )
                    .ObserveOn( RxApp.MainThreadScheduler );
            @this.CopyToClipboardCommand =
                ReactiveCommand.Create( () =>
                {
                    if ( @this.FullExport )
                        Clipboard.SetText( $"{@this.SelectedSource.Name} {@this.SelectedFlow.Ref} {@this.ResultingKey}" );
                    else
                        Clipboard.SetText( @this.ResultingKey );
                } , canCopy );

            @this.OpenInBrowserCommand =
                ReactiveCommand.CreateFromObservable( () => @this.OpenInBrowserInteraction.Handle( $"{@this.SelectedSource.Name} {@this.SelectedFlow.Ref} {@this.ResultingKey}" ) , canCopy );
        }

        private Func<FlowViewModel , bool> BuildFilter( string searchText )
        {
            if ( string.IsNullOrEmpty( searchText ) )
                return _ => true;

            return f => f.Ref.Contains( searchText , StringComparison.OrdinalIgnoreCase )
                        || f.Label.Contains( searchText , StringComparison.OrdinalIgnoreCase )
                        || string.CompareOrdinal( searchText , f.ToString() ) == 0;
        }
    }
}