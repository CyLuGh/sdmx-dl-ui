﻿using System;
using System.Collections.Generic;
using System.Text;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace sdmx_dl_ui.ViewModels
{
    public class CodeSelectorViewModel : ReactiveObject, IActivatableViewModel
    {
        public ViewModelActivator Activator { get; }

        public string Code { get; set; }
        public string Label { get; set; }

        [Reactive] public bool IsSelected { get; set; }

        public CodeSelectorViewModel()
        {
            Activator = new ViewModelActivator();
        }

    }
}
