## Introduction

This is a simple frontend for [sdmx-dl](https://github.com/nbbrd/sdmx-dl) library.

This is intended as a user controls library, to be integrated in other WPF applications or plugins.
