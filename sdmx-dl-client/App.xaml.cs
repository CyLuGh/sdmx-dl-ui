﻿using System.Windows;
using Splat;
using Splat.NLog;

namespace sdmx_dl_client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            ConfigureLogs();
            Locator.CurrentMutable.UseNLogWithWrappingFullLogger();
        }

        private void ConfigureLogs()
        {
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget( "logfile" )
            {
                FileName = "sdmx-dl-ui.log" ,
                DeleteOldFileOnStartup = true
            };

            var logconsole = new NLog.Targets.ConsoleTarget( "logconsole" );

            // Rules for mapping loggers to targets
            config.AddRule( NLog.LogLevel.Info , NLog.LogLevel.Fatal , logconsole );
            config.AddRule( NLog.LogLevel.Error , NLog.LogLevel.Fatal , logfile );

            // Apply config
            NLog.LogManager.Configuration = config;
        }

    }
}
